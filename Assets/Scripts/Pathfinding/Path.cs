﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class Path
    {
        Stack<AStarNode> nodeStack = new Stack<AStarNode>();

        public void Push(AStarNode node)
        {
            nodeStack.Push(node);
            node.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.0f, 1.0f, 0.0f);
        }

        public AStarNode Pop()
        {
            AStarNode node = nodeStack.Pop();
            node.gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f);
            return node;
        }

        public int Count
        {
            get
            {
                return nodeStack.Count;
            }
        }
        
        public void Delete()
        {
            while (nodeStack.Count > 0)
                this.Pop();
        }
    }
}
