﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Assets.Scripts
{
    public class AStarNode : MonoBehaviour, IComparable<AStarNode>
    {
        private AStarNode Parent;
        public AStarNode parent
        {
            get
            {
                return Parent;
            }
            set
            {
                Parent = value;
                value.child = this;
            }
        }
        [HideInInspector]
        public AStarNode child = null;

        public float cost;
        [HideInInspector]
        public float g;    //Cumulative cost from start to this node
        [HideInInspector]
        public float h;    //Heuristic estimate of cost to goal
        [HideInInspector]
        public float f //Sum of cost and heuristic
        {
            get
            {
                return g + h;
            }
        }
        public GridPosition gridPosition = new GridPosition ( -1, -1 ); //Initialised with invalid value so it can't accidentally be used

        [HideInInspector]
        public AStarNode[] neighbours = new AStarNode[] { null, null, null, null};
        
        public void Reset()
        {
            parent = null;
            child = null;
            g = h = 0.0f;
        }

        public void SetNeighbours()
        {
            //TODO - Possibly add a guard against neighbours being outside the map (not currenlty a problem on this game map)
            BoardManager bm = GameObject.Find("GameManager").GetComponent<BoardManager>();
            GameObject[,] worldMap = bm.worldGrid;
            if (this.gridPosition[0] > 0)
                neighbours[0] = worldMap[this.gridPosition[0] - 1   , this.gridPosition[1]      ].GetComponent<AStarNode>();
            if (this.gridPosition[1] < bm.rows - 1)
                neighbours[1] = worldMap[this.gridPosition[0], this.gridPosition[1] + 1].GetComponent<AStarNode>();
            if (this.gridPosition[0] < bm.columns - 1)
                neighbours[2] = worldMap[this.gridPosition[0] + 1   , this.gridPosition[1]      ].GetComponent<AStarNode>();
            if (this.gridPosition[1] > 0)
                neighbours[3] = worldMap[this.gridPosition[0]       , this.gridPosition[1] - 1  ].GetComponent<AStarNode>();
        }

        public int CompareTo(AStarNode other)
        {
            if (this.f > other.f)
                return 1;
            else if (this.f < other.f)
                return -1;
            else //They're equal
                return 0;
        }
    }
}
