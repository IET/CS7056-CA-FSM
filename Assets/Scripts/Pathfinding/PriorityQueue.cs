﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    class PriorityQueue<T> where T : IComparable<T>
    {
        List<T> queue = new List<T>();

        public PriorityQueue()
        {

        }

        public int Count
        {
            get
            {
                return queue.Count;
            }
        }

        //TODO - Insertion seems expensive...
        //Worst case = n
        public void Insert(T node)
        {
            bool isInserted = false;

            for (int i = 0; i < queue.Count; i++)
            {
                if (node.CompareTo(queue[i]) < 0)
                {
                    queue.Insert(i, node);
                    isInserted = true;
                    break;
                }
            }

            if (!isInserted)
            {
                queue.Add(node);
            }
        }

        //Constant time. 
        public void Delete(int i)
        {
            queue.RemoveAt(i);
        }

        //Worst case = n agan...
        //But do I ever delete something that's not head?
        public void Delete(T node)
        {
            queue.Remove(node);
        }

        //TODO - Should this be performing the deletion?
        public T GetMax()
        {
            T max = queue[0];
            queue.RemoveAt(0);
            return max;
        }

        //TODO - Is this one not O(n) too? What kind of lame ass data structure is this??
        public bool TestMemebership(T node)
        {
            return queue.Contains(node);// ? queue.FindIndex(node) : -1;
        }

        public void Clear()
        {
            queue.Clear();
        }
    }
}
