﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System; //Math.Abs

namespace Assets.Scripts
{
    public class AStarPathfinder {

        public delegate float Heuristic(AStarNode node, GridPosition target);

        //Need to implement binary heap for open list
/*
        public static AStarPathfinder _instance = new AStarPathfinder();

        public static AStarPathfinder instance
        {
            get
            {
                return _instance;
            }
        }
*/
        public AStarPathfinder() { }

        public void Reset()
        {
            //TODO - clear data
        }
        
        public Path FindPath(GridPosition start, GridPosition end)
        {
            return FindPath(start.x, start.y, end.x, end.y, DefaultHeuristic);
        }

        public Path FindPath(int startX, int startY, int endX, int endY, Heuristic heuristic)
        {
            Dictionary<GridPosition, AStarNode> closedList = new Dictionary<GridPosition, AStarNode>();
            PriorityQueue<AStarNode> openList = new PriorityQueue<AStarNode>();

            GridPosition start = new GridPosition(startX, startY);
            GridPosition goal = new GridPosition( endX, endY );

            GameObject[,] map = GameObject.Find("GameManager").GetComponent<BoardManager>().worldGrid;
            AStarNode startNode = map[startX, startY].GetComponent<Tile>();

            openList.Insert(startNode);
            AStarNode current = null;
            while (openList.Count > 0)
            {
                current = openList.GetMax();

                if (!closedList.ContainsKey(current.gridPosition))
                    closedList.Add(current.gridPosition, current);

                if (current.gridPosition == goal)
                {
                    //Path complete, haha!
                    break;
                }

                for (int i = 0; i < current.neighbours.Length; i++)
                {
                    AStarNode neighbour = current.neighbours[i];

                    if (neighbour == null)
                        continue;

                    //neighbour.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.0f, 0.0f);

                    float cost = current.g + neighbour.cost;
                    bool inOpen = openList.TestMemebership(neighbour);
                    bool inClosed = closedList.ContainsKey(neighbour.gridPosition);

                    //TODO - Implement early exit, as in this example https://github.com/justinhj/astar-algorithm-cpp/blob/master/cs/AStarPathfinder.cs

                    //Not sure this part is necessary since you'll only ever take the best one
                    if (inOpen && cost < neighbour.g)
                        openList.Delete(neighbour);

                    //If heuristic is inadmissible, this may occur
                    if (inClosed && cost < neighbour.g)
                        closedList.Remove(neighbour.gridPosition);

                    //TODO - Maybe change this based on the implementation here http://www.redblobgames.com/pathfinding/a-star/implementation.html#csharp
                    if (!openList.TestMemebership(neighbour) && !closedList.ContainsKey(neighbour.gridPosition))
                    {
                        neighbour.g = cost;
                        neighbour.h = heuristic(neighbour, goal);
                        neighbour.parent = current;

                        openList.Insert(neighbour);
                        //closedList.Add(current.gridPosition, current);
                        //break;
                    }
                }
            }

            //Is this not a hugely inefficient way of returning the path??
            Path path = new Path();
            AStarNode node = current;
            while (node != startNode)
            {
                path.Push(node);
                node = node.parent;
            }

            return path;
        }

        public AStarNode FindSensePath(int startX, int startY, int endX, int endY, float range)
        {
            return FindSensePath( startX,  startY,  endX, endY , range, DefaultHeuristic);
        }

        public AStarNode FindSensePath(int startX, int startY, int endX, int endY, float range, Heuristic heuristic)
        {
            Dictionary<GridPosition, AStarNode> closedList = new Dictionary<GridPosition, AStarNode>();
            PriorityQueue<AStarNode> openList = new PriorityQueue<AStarNode>();

            GridPosition start = new GridPosition(startX, startY);
            GridPosition goal = new GridPosition(endX, endY);

            GameObject[,] map = GameObject.Find("GameManager").GetComponent<BoardManager>().worldGrid;
            AStarNode startNode = map[startX, startY].GetComponent<SensoryAttenuator>();

            openList.Insert(startNode);
            AStarNode current = null;
            while (openList.Count > 0)
            {
                current = openList.GetMax();
                closedList.Add(current.gridPosition, current);

                if (current.gridPosition == goal)
                {
                    //Path complete, haha!
                    break;
                }

                for (int i = 0; i < current.neighbours.Length; i++)
                {
                    AStarNode neighbour = current.neighbours[i];

                    if (neighbour == null)
                        continue;

                    //neighbour.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.0f, 0.0f);

                    float cost = current.g + neighbour.cost;
                    bool inOpen = openList.TestMemebership(neighbour);
                    bool inClosed = closedList.ContainsKey(neighbour.gridPosition);

                    //TODO - Implement early exit, as in this example https://github.com/justinhj/astar-algorithm-cpp/blob/master/cs/AStarPathfinder.cs

                    if (inOpen && cost < neighbour.g)
                        openList.Delete(neighbour);

                    //If heuristic is inadmissible, this may occur
                    if (inClosed && cost < neighbour.g)
                        closedList.Remove(neighbour.gridPosition);

                    //TODO - Maybe change this based on the implementation here http://www.redblobgames.com/pathfinding/a-star/implementation.html#csharp
                    if (!openList.TestMemebership(neighbour) && !closedList.ContainsKey(neighbour.gridPosition))
                    {
                        neighbour.g = cost;
                        neighbour.h = heuristic(neighbour, goal);
                        neighbour.parent = current;

                        openList.Insert(neighbour);
                        //closedList.Add(current.gridPosition, current);
                        //break;
                    }
                }
            }

            return current;
        }

        private float DefaultHeuristic(AStarNode node, GridPosition target)
        {
            node.h = ManhattanDistance(node.gridPosition, target);
            return node.h;
        }

        private float ManhattanDistance(GridPosition pos, GridPosition target)
        {
            return Math.Abs(target.x - pos.x) + Math.Abs(target.y - pos.y); 
        }

        private float ManhattanDistance(int startX, int startY, int endX, int endY)
        {
            return Math.Abs(endX - startX) + Math.Abs(endY - startY);
        }
    }
}
