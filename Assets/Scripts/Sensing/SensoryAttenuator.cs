﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    class SensoryAttenuator : AStarNode
    {
        //TODO - Return different attenuation values for different sensory modalities
        public float attenuation
        {
            get
            {
                return this.cost;
            }

            set
            {
                this.cost = value;
            }
        }

        new public void SetNeighbours()
        {
            //TODO - Possibly add a guard against neighbours being outside the map (not currenlty a problem on this game map)
            BoardManager bm = GameObject.Find("GameManager").GetComponent<BoardManager>();
            GameObject[,] worldMap = bm.worldGrid;
            if (this.gridPosition[0] > 0)
                neighbours[0] = worldMap[this.gridPosition[0] - 1, this.gridPosition[1]].GetComponent<SensoryAttenuator>();
            if (this.gridPosition[1] < bm.rows - 2)
                neighbours[1] = worldMap[this.gridPosition[0], this.gridPosition[1] + 1].GetComponent<SensoryAttenuator>();
            if (this.gridPosition[0] < bm.columns - 2)
                neighbours[2] = worldMap[this.gridPosition[0] + 1, this.gridPosition[1]].GetComponent<SensoryAttenuator>();
            if (this.gridPosition[1] > 0)
                neighbours[3] = worldMap[this.gridPosition[0], this.gridPosition[1] - 1].GetComponent<SensoryAttenuator>();
        }

    }
}
