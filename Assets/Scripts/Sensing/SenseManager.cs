using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class SenseManager
    {
        AStarPathfinder aStarPathfinder = new AStarPathfinder();
        private static SenseManager _instance = new SenseManager();

        //Priority Queue of sense events
        //Second parameter is not needed but C# seems to have no direct equivalent to std::set in C++
        SortedDictionary<SenseNotification, bool> PriorityQ = new SortedDictionary<SenseNotification, bool>();

        private Dictionary<int, Sensor> sensors = new Dictionary<int, Sensor>();

        public static SenseManager instance
        {
            get
            {
                return _instance;
            }
        }
        protected SenseManager() { }

        //Register a Sensor with the sense manager
        public void AddSensor(int id, Sensor sensor)
        {
            sensors.Add(id, sensor);
        }

        public void AddSignal(Signal signal)
        {
            List<Sensor> validSensors = new List<Sensor>();

            AStarPathfinder.Heuristic heuristic = SightHeuristic;

            foreach (Sensor sensor in sensors.Values)
            {
                if (!sensor.DetectsModality(signal.modality))
                    continue;

                if (sensor.GetId() == signal.sourceId)
                    continue;

                AStarNode pathEnd = aStarPathfinder.FindSensePath(signal.source.x, signal.source.y, sensor.gridPosition.x, sensor.gridPosition.y, signal.modality.range, heuristic);

                float distance = GridPosition.ManhattanDistance(pathEnd.gridPosition, signal.source);
                if (distance > signal.modality.range)
                    continue;

                float intensity = signal.intensity * Mathf.Pow(1.0f/pathEnd.g, distance);

                if (intensity < sensor.GetThreshold(signal.modality))
                    continue;

                float notificationTime = Time.time + distance * signal.modality.inverseTransmissionSpeed;

                SenseNotification notification = new SenseNotification(notificationTime, sensor, signal);

                PriorityQ.Add(notification, true);
            }
        }
        
        //In the future it may make sense to have this as part of the modality class
        //Pure BFS for sight as it propogates in straight lines
        float SightHeuristic(AStarNode node, GridPosition target)
        {
            //Times 20 because 20 >> max cost
            return 20 * GridPosition.ManhattanDistance(node.gridPosition, target);
        }

        //I think sight and smell could just use standard A*


        public void SendSignals()
        {
            //TODO - Confirm time offset works
            float currentTime = Time.time;


            while (PriorityQ.Count > 0 &&
                PriorityQ.First().Key.time < currentTime &&
                    PriorityQ.First().Key.time > 0)
            {
                SenseNotification not = PriorityQ.First().Key;

                not.sensor.HandleSenseEvent(not);

                PriorityQ.Remove(not);
            }

        }
       
    }
}
