﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public abstract class Sensor : Agent
    {
        float threshold = 10.0f;
        //By default all modalities are ignored. A sensor must implement a version of this function for specific modalities
        abstract public bool DetectsModality(Modality m);
        
        //Similar to the above, sensors can implement additional thresholds specific to a modality
        public float GetThreshold(Modality m)
        {
            return this.threshold;
        }

        abstract public bool HandleSenseEvent(SenseNotification not);

    }
}
