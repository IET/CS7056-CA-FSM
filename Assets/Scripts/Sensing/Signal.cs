using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Assets.Scripts
{
    public class Signal
    {
    	//Used to define how they propogate
        public Modality modality;
        public float intensity;
        public GridPosition source;
        public int sourceId;

        public Signal(Modality m, float intensity, GridPosition signalSource, int sourceId)
        {
            modality = m;
            this.intensity = intensity;
            this.source = signalSource;
            this.sourceId = sourceId;
        }
    }
}
