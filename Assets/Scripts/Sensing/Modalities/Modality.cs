﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{ 
    public class Modality
    {
        public float range;
        public float attenuation;  //Constant
        public float inverseTransmissionSpeed;

        public bool ExtraChecks(Signal signal, Sensor sensor)
        {
            //Specific modalities can override this if they have some extra checks needed (sight cones, etc.)
            return true;
        }
    }
}
