﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class SightModality : Modality
    {
        public SightModality()
        {
            this.attenuation = 0.1f;
            this.range = 1000;
            this.inverseTransmissionSpeed = 0.01f;
        }
    }
}
