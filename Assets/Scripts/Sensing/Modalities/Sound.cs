﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class SoundModality : Modality
    {
        public SoundModality()
        {
            attenuation = 1.0f;
            range = 15.0f;
            inverseTransmissionSpeed = 0.1f; //Unfortunately my gameworld doesn't have reasonable units
        }

    }
}
