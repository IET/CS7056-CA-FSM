﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class SmellModality : Modality
    {
        public SmellModality()
        {
            this.attenuation = 0.05f;
            this.range = 10.0f;
            this.inverseTransmissionSpeed = 1.0f;
        }
    }
}
