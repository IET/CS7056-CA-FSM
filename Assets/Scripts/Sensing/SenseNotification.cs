﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class SenseNotification : IComparable
    {
        public float time;
        public Sensor sensor;
        public Signal signal;

        public SenseNotification(float time, Sensor s, Signal sig)
        {
            this.time = time;
            sensor = s;
            signal = sig;
        }

        public static bool operator ==(SenseNotification x, SenseNotification y)
        {
            return (x.time - y.time) < 0.25;
        }

        public static bool operator !=(SenseNotification x, SenseNotification y)
        {
            return !((x.time - y.time) < 0.25);
        }

        public static bool operator <(SenseNotification x, SenseNotification y)
        {
            return x.time < y.time;
        }

        public static bool operator >(SenseNotification x, SenseNotification y)
        {
            return x.time > y.time;
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            SenseNotification otherSenseNotification = obj as SenseNotification;

            if (this.time == otherSenseNotification.time)
                return 0;
            else if (this.time < otherSenseNotification.time)
                return -1;
            else return 1;
        }
    }
}
