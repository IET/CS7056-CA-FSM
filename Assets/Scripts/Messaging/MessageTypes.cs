﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    enum message_type
    {
        msg_hi_honey_im_home,
        msg_dinner_ready,
        msg_stop,           //Used by outlaw to stop agents he catches
        msg_trouble,        //Used by outlaw to cause trouble
        msg_no_trouble,     //Bob explains he doesn't want any trouble
        msg_my_house,       //Bob is aghast that the outlaw has come to his house!
        msg_carry_on,        //Outlaw leaves
        msg_bedtime,        //Otherwise Elsa never sleeps
        msg_wakeup
    };
}
