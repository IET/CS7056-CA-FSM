﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class Message : IComparable
    {
        public Message(float dispatchTime, int sender, int receiver, int message)
        {
            this.dispatchTime = dispatchTime;
            this.sender = sender;
            this.receiver = receiver;
            this.msg = message;
        }

        //ID of the entity sending the message
        public int sender;

        //ID of the message recipient
        public int receiver;

        //ID of the message. Messages will need to be enumerted.
        public int msg;

        //Time to deliver message. Allows for delayed messages.
        //TODO - Guard against anti-dated messages.
        public float dispatchTime;

        //Extra info
        //TODO - Figure out how to implement this is C#
        //void* extraInfo;

        //Comparison operators used for maintaining ordered list
        public static bool operator == (Message x, Message y)
        {
            return (x.dispatchTime - y.dispatchTime) < 0.25;
        }

        public static bool operator != (Message x, Message y)
        {
            return !((x.dispatchTime - y.dispatchTime) < 0.25);
        }

        public static bool operator < (Message x, Message y)
        {
            return x.dispatchTime < y.dispatchTime;
        }

        public static bool operator > (Message x, Message y)
        {
            return x.dispatchTime > y.dispatchTime;
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Message otherMessage = obj as Message;

            if (this.dispatchTime == otherMessage.dispatchTime)
                return 0;
            else if (this.dispatchTime < otherMessage.dispatchTime)
                return -1;
            else return 1;
        }
    }
}
