﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class MessageDispatcher
    {
        //Allows agents to be looked up by name. 
        //Note: Multiple agents may have same name so messages will be dispatched to any agent with that name
        public Dictionary<string, List<int>> phoneBook = new Dictionary<string, List<int>>();

        //Keep anyone from using constructor
        protected MessageDispatcher() { }

        //Priority Queue of delayed messages
        //Second parameter is not needed but C# seems to have no direct equivalent to std::set in C++
        SortedDictionary<Message, bool> PriorityQ = new SortedDictionary<Message, bool>();

        //Calls message handling function of receiver with the message
        private void Discharge(Agent receiver, Message msg)
        {
            receiver.HandleMessage(msg);
        }

        public static MessageDispatcher _instance = new MessageDispatcher();

        public static MessageDispatcher instance
        {
            get
            {
                return _instance;
            }
        }

        public void AddAgent(string name, int id)
        {
            if (phoneBook.ContainsKey(name))
            {
                phoneBook[name].Add(id);
            }
            else
            {
                phoneBook[name] = new List<int>();
                phoneBook[name].Add(id);
            }
        }

        //Allows you to send a message to all agents with a particular name
        public void DispatchMessageByName(float delay, int sender, string receiver, int msg/*,void * extraInfo*/)
        {
            if (!phoneBook.ContainsKey(receiver))
            {
                Debug.Log("Receiver not found, message not sent.");
                return;
            }

            for (int i = 0; i < phoneBook[receiver].Count(); i++)
            {
                Agent destinationAgent = AgentManager.instance.GetAgentWithId(phoneBook[receiver][i]);
                Message message = new Message(0, sender, phoneBook[receiver][i], msg);

                if (delay <= 0.0)
                {
                    Discharge(destinationAgent, message);
                }
                else
                {
                    //TODO - Confirm time offset works
                    float currentTime = Time.time;

                    message.dispatchTime = currentTime + delay;

                    //TODO - Confim queue is sorted
                    PriorityQ.Add(message, true);
                }
            }
        }

        public void DispatchMessage(float delay, int sender, int receiver, int msg/*,void * extraInfo*/)
        {
            Agent destinationAgent = AgentManager.instance.GetAgentWithId(receiver);

            Message message = new Message(0, sender, receiver, msg);

            if (delay <= 0.0)
            {
                Discharge(destinationAgent, message);
            }
            else
            {
                //TODO - Confirm time offset works
                float currentTime = Time.time;

                message.dispatchTime = currentTime + delay;

                //TODO - Confim queue is sorted
                PriorityQ.Add(message, true);
            }
        }

        //Send out delayed messages, called once per game loop
        //TODO - Implement
        public void DispatchDelayedMessages()
        {
            float currentTime = Time.time;
            
            while (PriorityQ.Count() > 0 && 
                PriorityQ.First().Key.dispatchTime < currentTime &&
                    PriorityQ.First().Key.dispatchTime > 0)
            {
                Message msg = PriorityQ.First().Key;

                Agent receiver = AgentManager._instance.GetAgentWithId(msg.receiver);

                Discharge(receiver, msg);

                PriorityQ.Remove(msg);
            }
            
        }
    }
}
