﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;       //Allows us to use Lists. 

namespace Assets.Scripts
{
    public class GameManager : MonoBehaviour
    {

        public static GameManager instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.
        private BoardManager boardScript;                       //Store a reference to our BoardManager which will set up the level.
        private int level = 3;                                  //Current level number, expressed in game as "Day 1".

        //Awake is always called before any Start functions
        void Awake()
        {
            //Check if instance already exists
            if (instance == null)

                //if not, set instance to this
                instance = this;

            //If instance already exists and it's not this:
            else if (instance != this)

                //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
                Destroy(gameObject);

            //Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);

            //Get a component reference to the attached BoardManager script
            boardScript = GetComponent<BoardManager>();

            //Call the InitGame function to initialize the first level 
            InitGame();

            //Move Bob
            GameObject.Find("Bob").GetComponent<Bob>().gridPosition = new GridPosition( Random.Range(1, (boardScript.columns / 2)), Random.Range(1, (boardScript.rows / 2)) );
            //Move Outlaw
            GameObject.Find("Outlaw").GetComponent<Outlaw>().gridPosition = new GridPosition(Random.Range(1, (boardScript.columns / 2)), Random.Range(1, (boardScript.rows / 2)));
            //Send Elsa Home
            GameObject.Find("Elsa").GetComponent<Elsa>().gridPosition = boardScript.gridMap["shack"];

        }

        //Initializes the game for each level.
        void InitGame()
        {
            //Call the SetupScene function of the BoardManager script, pass it current level number.
            boardScript.SetupScene(level);

        }



        //Update is called every frame.
        void Update()
        {
            MessageDispatcher._instance.DispatchDelayedMessages();
            SenseManager.instance.SendSignals();
        }
    }
}