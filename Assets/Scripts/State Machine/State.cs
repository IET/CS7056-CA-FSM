﻿namespace Assets.Scripts
{
    abstract public class State<T>
    {

        abstract public void Enter(T agent);
        abstract public void Execute(T agent);
        abstract public void Exit(T agent);
        abstract public bool OnMessage(T agent, Message m);
        abstract public bool OnSenseEvent(T agent, SenseNotification not);

    }
}