﻿using UnityEngine;

namespace Assets.Scripts
{
    public class StateMachine<T> where T : Agent
    {

        private T agent;

        private State<T> currentState;
        private State<T> previousState;
        private State<T> globalState;

        public StateMachine()
        {
            agent           = default(T);
            currentState    = null;
            previousState   = null;
            globalState     = null;
        }
        

        public StateMachine(T owner)
        {
            agent           = owner;
            currentState    = null;
            previousState   = null;
            globalState     = null;
        }
        

        public void Awake()
        {
            this.currentState = null;
        }

        public void Init(T agent, State<T> startState)
        {
            this.agent = agent;
            this.currentState = startState;
        }

        public void Init(T agent, State<T> startState, State<T> globalState)
        {
            this.agent = agent;
            this.ChangeState(startState);
            this.globalState = globalState;
        }

        public void Update()
        {
            BoardManager boardScript = GameObject.Find("GameManager").GetComponent<BoardManager>();
            Vector3 position = boardScript.worldGrid[agent.gridPosition.x, agent.gridPosition.y].transform.position;
            agent.transform.position = new Vector3(position.x, position.y, agent.transform.position.z);

            if (agent.currentPath != null && agent.currentPath.Count > 0)
            {
                agent.currMovementCost -= (agent.movementSpeed * Time.deltaTime);
                if (agent.currMovementCost <= 0)
                {
                    AStarNode node = agent.currentPath.Pop();
                    agent.currMovementCost = node.cost;
                    agent.Move(node.gridPosition);
                }
            }

            if (globalState != null)
                this.globalState.Execute(this.agent);

            if (this.currentState != null)
                this.currentState.Execute(this.agent);
        }

        public void ChangeState(State<T> nextState)
        {
            if (nextState == currentState)
                return;

            if (this.currentState != null)
            {
                this.previousState = this.currentState;
                this.currentState.Exit(this.agent);
            }

            this.currentState = nextState;

            //TODO - Should null input to this function actualy be valid?
            if (this.currentState != null)
                this.currentState.Enter(this.agent);
        }

        public void RevertToPreviousState()
        {
            if (this.previousState != null)
            {
                ChangeState(this.previousState);
            }
        }

        public State<T> getCurrentState() {
            return this.currentState;
        }

        public State<T> getGlobalState()
        {
            return this.globalState;
        }

        public State<T> getPreviousState()
        {
            return this.previousState;
        }

        public bool isInState(State<T> st)
        {
            return currentState == st;
        }

        public bool HandleMessage(Message m)
        {
            if ((currentState != null) && currentState.OnMessage(agent, m))
            {
                return true;
            }
            else if ((globalState != null) && globalState.OnMessage(agent, m))
            {
                return true;
            }

            return false;
        }

        public bool HandleSenseEvent(SenseNotification not)
        {
            if ((currentState != null) && currentState.OnSenseEvent(agent, not))
            {
                return true;
            }
            else if ((globalState != null) && globalState.OnSenseEvent(agent, not))
            {
                return true;
            }

            return false;
        }
    }
}