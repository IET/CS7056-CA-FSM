﻿using UnityEngine;
using System;
using System.Collections.Generic;       //Allows us to use Lists.
using Random = UnityEngine.Random;      //Tells Random to use the Unity Engine random number generator.

namespace Assets.Scripts
{
    //TODO - Implement water, should be grouped together fairly randomly to make lakes and rivers (in the desert...)

    public class BoardManager : MonoBehaviour
    {
        // Using Serializable allows us to embed a class with sub properties in the inspector.
        [Serializable]
        public class Count
        {
            public int minimum;             //Minimum value for our Count class.
            public int maximum;             //Maximum value for our Count class.


            //Assignment constructor.
            public Count(int min, int max)
            {
                minimum = min;
                maximum = max;
            }
        }

        public Vector2 startingPosition = new Vector2(-16, -9);
        public float tileSize = 1.5f;
        public int columns = 25;                                        //Number of columns in our game board.
        public int rows = 8;                                            //Number of rows in our game board.
        public Count mountainCount = new Count(5, 9);                       //Lower and upper limit for our random number of walls per level.
        public Count waterTileCount = new Count(6, 11);
        public GameObject shack;                                        //Prefab to spawn for exit.
        public GameObject bar;                                          //Prefab to spawn for bar.
        public GameObject mine;                                         //Prefab to spawn for mine.

        public GameObject[,] worldGrid;
        public GameObject[] floorTiles;                                 //Array of floor prefabs.
        public GameObject[] mountainTiles;                                  //Array of wall prefabs.
        public GameObject[] outerWallTiles;                             //Array of outer tile prefabs.
        public GameObject waterTile;

        //TODO - Implement a good way for Agents to find these locations
        public GridPosition shackLocation;
        public GridPosition mineLocation;
        public Dictionary<string, GridPosition> gridMap = new Dictionary<string, GridPosition>();

        private Transform boardHolder;                                  //A variable to store a reference to the transform of our Board object.
        private List<Vector3> gridPositions = new List<Vector3>();      //A list of possible locations to place tiles.

        //Clears our list gridPositions and prepares it to generate a new board.
        void InitialiseList()
        {
            //Clear our list gridPositions.
            gridPositions.Clear();

            //Loop through x axis (columns).
            for (int x = 0; x < columns; x++)
            {
                //Within each column, loop through y axis (rows).
                for (int y = 0; y < rows; y++)
                {
                    //At each index add a new Vector3 to our list with the x and y coordinates of that position.
                    gridPositions.Add(new Vector3(startingPosition.x + x * tileSize, startingPosition.y + y * tileSize, 0f));
                }
            }
        }

        //Sets up the outer walls and floor (background) of the game board.
        void BoardSetup()
        {
            worldGrid = new GameObject[columns, rows];

            //Instantiate Board and set boardHolder to its transform.
            boardHolder = new GameObject("Board").transform;

            //Loop along x axis, starting from -1 (to fill corner) with floor or outerwall edge tiles.
            for (int x = 0; x < columns; x++)
            {
                //Loop along y axis, starting from -1 to place floor or outerwall tiles.
                for (int y = 0; y < rows; y++)
                {
                    //Choose a random tile from our array of floor tile prefabs and prepare to instantiate it.
                    GameObject toInstantiate = floorTiles[Random.Range(0, floorTiles.Length)];

                    //Check if current position is at board edge, if so choose a random outer wall prefab from our array of outer wall tiles.
                    if (x == 0 || x == columns -1|| y == 0 || y == rows -1)
                        toInstantiate = outerWallTiles[Random.Range(0, outerWallTiles.Length)];

                    //Instantiate the GameObject instance using the prefab chosen for toInstantiate at the Vector3 corresponding to current grid position in loop, cast it to GameObject.
                    GameObject instance =
                        Instantiate(toInstantiate, new Vector3(startingPosition.x + (x * tileSize), startingPosition.y + (y * tileSize), 0f), Quaternion.identity) as GameObject;

                    //Set the parent of our newly instantiated object instance to boardHolder, this is just organizational to avoid cluttering hierarchy.
                    instance.transform.SetParent(boardHolder);

                    instance.GetComponent<Tile>().x = x;
                    instance.GetComponent<Tile>().y = y;

                    worldGrid[x, y] = instance;
                }
            }
        }

        GridPosition RandomIndex()
        {
            return new GridPosition((int)Random.Range(1, columns - 2), (int)Random.Range(1, rows - 2));
        }


        //LayoutObjectAtRandom accepts an array of game objects to choose from along with a minimum and maximum range for the number of objects to create.
        void LayoutObjectAtRandom(GameObject[] tileArray, int minimum, int maximum)
        {
            //Choose a random number of objects to instantiate within the minimum and maximum limits
            int objectCount = Random.Range(minimum, maximum);

            //Instantiate objects until the randomly chosen limit objectCount is reached
            for (int i = 0; i < objectCount; i++)
            {
                //Choose a position for randomPosition by getting a random position from our list of available Vector3s stored in gridPosition
                //Vector3 randomPosition = RandomPosition();

                GridPosition randomIndex = RandomIndex();

                //Find a new reandom position if a special tile is located here
                while (gridMap.ContainsValue(randomIndex))
                    randomIndex = RandomIndex();
                
                try {
                    Vector3 randomPosition = gridPositions[(randomIndex.x) + (randomIndex.y * rows)];
                }
                catch
                {
                    Debug.LogError("Error accessing index: [" + ((randomIndex.x) + (randomIndex.y * rows)).ToString() + "] in array of size: " + gridPositions.Count);
                }
                //Choose a random tile from tileArray and assign it to tileChoice
                GameObject tileChoice = tileArray[Random.Range(0, tileArray.Length)];
                gridPositions.RemoveAt(randomIndex.x + (randomIndex.y * rows));
                GameObject.Destroy(worldGrid[randomIndex.x,randomIndex.y]);
                //Instantiate tileChoice at the position returned by RandomPosition with no change in rotation
                
                GameObject instance = Instantiate(tileChoice, new Vector3(startingPosition.x + (randomIndex.x * tileSize), startingPosition.y + (randomIndex.y * tileSize), 0f), Quaternion.identity) as GameObject;
                instance.transform.SetParent(boardHolder);

                instance.GetComponent<Tile>().x = randomIndex.x;
                instance.GetComponent<Tile>().y = randomIndex.y;

                //If this is a unique object, add it to the map
                if ((minimum == maximum) && (maximum == 1))
                {
                    string locale = tileChoice.gameObject.name;
                    string[] splitLocale = locale.Split('_');
                    gridMap.Add(splitLocale[splitLocale.Length - 1], new GridPosition ( randomIndex.x, randomIndex.y ));
                }  

                //worldGrid[randomIndex.x, randomIndex.y] = new GameObject();
                worldGrid[randomIndex.x, randomIndex.y] = instance;
            }
        }

        //Water tiles are placed differently so they can clump together into lakes and rivers
        //This implementation means that water will just plow through any other (non-special) tiles
        private void PlaceWater(int numWaterTiles)
        {
            int numTiles = 0;
            while (numTiles < numWaterTiles)
            {
                int numTilesToAdd = (int)Random.Range(1, numWaterTiles);
                

                GridPosition index = RandomIndex();

                //Don't place on edges or ouside of map
                if (index.x < 1 || index.x > columns - 2 || index.y < 1 || index.y > rows - 2)
                    break;

                //Also avoid special locations
                if (gridMap.ContainsValue(index))
                    break;

                gridPositions.RemoveAt(index.x + (index.y * rows));
                GameObject.Destroy(worldGrid[index.x, index.y]);

                GameObject instance = Instantiate(waterTile, new Vector3(startingPosition.x + (index.x * tileSize), startingPosition.y + (index.y * tileSize), 0f), Quaternion.identity) as GameObject;
                instance.transform.SetParent(boardHolder);

                instance.GetComponent<Tile>().x = index.x;
                instance.GetComponent<Tile>().y = index.y;

                worldGrid[index.x, index.y] = instance;

                int numAddedTiles = 1;


                while (numAddedTiles < numTilesToAdd)
                {
                    int direction = (int)Random.Range(0,3);

                    switch (direction)
                    {
                        case 0:
                            ++index.x;
                            break;
                        case 1:
                            --index.y;
                            break;
                        case 2:
                            --index.x;
                            break;
                        case 3:
                            ++index.y;
                            break;
                    }

                    //Don't place on edges or ouside of map
                    if (index.x < 1 || index.x > columns -2 || index.y < 1 || index.y > rows -2)
                        break;

                    //Also avoid special locations
                    if (gridMap.ContainsValue(index))
                        break;

                    gridPositions.RemoveAt(index.x + (index.y * rows));
                    GameObject.Destroy(worldGrid[index.x, index.y]);

                    instance = Instantiate(waterTile, new Vector3(startingPosition.x + (index.x * tileSize), startingPosition.y + (index.y * tileSize), 0f), Quaternion.identity) as GameObject;
                    instance.transform.SetParent(boardHolder);

                    instance.GetComponent<Tile>().x = index.x;
                    instance.GetComponent<Tile>().y = index.y;

                    worldGrid[index.x, index.y] = instance;

                    numAddedTiles++;
                }

                numTiles += numAddedTiles;

            }

        }

        //SetupScene initializes our level and calls the previous functions to lay out the game board
        public void SetupScene(int level)
        {
            //Creates the outer walls and floor.
            BoardSetup();

            //Reset our list of gridpositions.
            InitialiseList();

            //Instantiate a random number of wall tiles based on minimum and maximum, at randomized positions.
            LayoutObjectAtRandom(mountainTiles, mountainCount.minimum, mountainCount.maximum);

            LayoutObjectAtRandom(mountainTiles, mountainCount.minimum, mountainCount.maximum);

            LayoutObjectAtRandom(new GameObject[]{mine}, 1, 1);
            LayoutObjectAtRandom(new GameObject[] { bar }, 1, 1);
            LayoutObjectAtRandom(new GameObject[] { shack }, 1, 1);
            PlaceWater((int)Random.Range(waterTileCount.minimum, waterTileCount.maximum));

            //Determine number of enemies based on current level number, based on a logarithmic progression
            int enemyCount = (int)Mathf.Log(level, 2f);

            //Set locations for sensory attenuastors because I am not a smart man
            for (int i = 0; i < columns; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    worldGrid[i, j].GetComponent<SensoryAttenuator>().gridPosition =
                        worldGrid[i, j].GetComponent<Tile>().gridPosition;
                }
            }
            
            //Set all the neighbour information
            for (int i =0; i < columns; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    worldGrid[i, j].GetComponent<Tile>().SetNeighbours();
                    SensoryAttenuator sa = worldGrid[i, j].GetComponent<SensoryAttenuator>();
                    if (sa != null)
                    {
                        sa.SetNeighbours();
                    }
                }
            }
        }
    }
}