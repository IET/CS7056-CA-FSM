﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class GridPosition
    {
        public int x;
        public int y;

        public GridPosition()
        {
            x = new int();
            y = new int();
        }
        public GridPosition(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public GridPosition(GridPosition other)
        {
            this.x = other.x;
            this.y = other.y;
        }

        public override bool Equals(object obj)
        {
            GridPosition other = obj as GridPosition;
            return (other.x == this.x && other.y == this.y);
        }

        public static  bool operator ==(GridPosition a, GridPosition b)
        {
            //Why does testing for null just not work??
/*
            if (System.Object.ReferenceEquals(a, b))
                return true;
            if (System.Object.ReferenceEquals(a, null) || System.Object.ReferenceEquals(null, b))
                return false;
*/
            return ((a.x == b.x) && (a.y == b.y));
        }

        public static bool operator !=(GridPosition a, GridPosition b)
        {
            return (a.x != b.x || a.y != b.y);
        }
        
        public static GridPosition operator +(GridPosition a, GridPosition b)
        {
            return new GridPosition(a.x + b.x, a.y + b.y);
        }

        public static GridPosition operator -(GridPosition a, GridPosition b)
        {
            return new GridPosition(a.x - b.x, a.y - b.y);
        }

        public override int GetHashCode()
        {
            return (7 * x.GetHashCode() + y.GetHashCode());
        }

        public int this[int key]
        {
            get
            {
                if (key > 1 || key < 0)
                {
                    Debug.LogError("Attempting to access an invalid index of a GridPosition Datatype");
                    throw (new IndexOutOfRangeException());
                }
                else if (key == 0)
                {
                    return x;
                }
                else
                {
                    return y;
                }
            }

            set
            {
                if (key > 1 || key < 0)
                {
                    Debug.LogError("Attempting to access an invalid index of a GridPosition Datatype");
                    throw (new IndexOutOfRangeException());
                }
                else if (key == 0)
                {
                    x = value;
                }
                else
                {
                    y =  value;
                }
            }
        }

        static public float ManhattanDistance(GridPosition a, GridPosition b)
        {
            return Math.Abs(a.x - b.x) + Math.Abs(a.y - b.y);
        }

        public bool isValid()
        {
            BoardManager bm = GameObject.Find("GameManager").GetComponent<BoardManager>();
            if (x < 1 || x > bm.columns - 2 || y < 1 || y > bm.rows - 2)
            {
                return false;
            }
            else
                return true;
        }
    }
}
