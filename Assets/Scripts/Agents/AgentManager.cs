﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    //Defining EntityMap to be a dictionary of int, agent pairs
    using AgentMap = System.Collections.Generic.Dictionary<int, Agent>;

    //sealed means nothing can inherit
    sealed class AgentManager
    {
        private AgentMap agentMap = new AgentMap();

        public AgentManager()
        {

        }

        private AgentManager(AgentManager rhs)
        {
            //TOOD - Copy constructor
        }


        public static AgentManager _instance = new AgentManager();

        public static AgentManager instance
        {
            get
            {
                return _instance;
            }
        }

        public void RegisterAgent(Agent newAgent)
        {
            _instance.agentMap.Add(newAgent.GetId(), newAgent);
        }

        public Agent GetAgentWithId(int idIn)
        {
            return agentMap[idIn];
        }

        public void RemoveAgent(Agent agent)
        {
            agentMap.Remove(agent.GetId());
        }
    }
}
