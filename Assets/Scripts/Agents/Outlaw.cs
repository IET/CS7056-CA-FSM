﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class Outlaw : Sensor
    {
        new public StateMachine<Outlaw> stateMachine;

        [HideInInspector]
        public int target;

        //Drink whiskey every 15s
        public float invDrinkingSpeed = 30.0f;

        public void Awake()
        {
            this.currentPath = new Path();

            this.stateMachine = new StateMachine<Outlaw>(this);
            this.stateMachine.Init(this, WanderState.Instance, OutlawGlobalState.Instance);

            SenseManager.instance.AddSensor(this.id, this);

            agentName = "Outlaw";
            MessageDispatcher._instance.AddAgent(this.agentName, this.GetId());
        }

        public void ChangeState(State<Outlaw> state)
        {
            this.stateMachine.ChangeState(state);
        }

        public override void RevertToPreviousState()
        {
            this.stateMachine.RevertToPreviousState();
        }

        new public void Update()
        {
            this.stateMachine.Update();
        }

        public override bool HandleMessage(Message m)
        {
            return stateMachine.HandleMessage(m);
        }

        public override bool HandleSenseEvent(SenseNotification not)
        {
            return stateMachine.HandleSenseEvent(not);
        }

        public bool DetectsModality(SightModality s)
        {
            return true;
        }
        public override bool DetectsModality(Modality m)
        {
            return true;
        }
    }
}
