﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class Elsa : Agent
    {
        new public StateMachine<Elsa> stateMachine;
        public bool isCooking = false;

        public void Awake()
        {
            this.stateMachine = new StateMachine<Elsa>(this);
            this.stateMachine.Init(this, DoHouseWork.Instance, ElsaGlobalState.Instance);

            agentName = "Elsa";
            MessageDispatcher._instance.AddAgent(this.agentName, this.GetId());
        }

        public void ChangeState(State<Elsa> state)
        {
            this.stateMachine.ChangeState(state);
        }

        public override void RevertToPreviousState()
        {
            this.stateMachine.RevertToPreviousState();
        }

        new public void Update()
        {
            this.stateMachine.Update();
        }

        public override bool HandleMessage(Message m)
        {
            return stateMachine.HandleMessage(m);
        }
/*
        public override bool HandleSenseEvent(SenseNotification not)
        {
            return stateMachine.HandleSenseEvent(not);
        }

        public override bool DetectsModality(Modality m)
        {
            return false;
        }
*/
    }
}
