﻿using UnityEngine;
using System.Collections;
using System;

namespace Assets.Scripts
{

    public class Bob : Agent
    {
        new public StateMachine<Bob> stateMachine;

        [HideInInspector]
        public float fatigue = 0;
        [HideInInspector]
        public int goldNuggets = 0;

        protected BoardManager boardScript;

        public void Awake()
        {
            boardScript = GameObject.Find("GameManager").GetComponent<BoardManager>();
            
            this.stateMachine = new StateMachine<Bob>(this);
            this.stateMachine.Init(this, WorkState.Instance, BobGlobalState.Instance);

            movementSpeed = 2.0f;

            agentName = "Bob";
            MessageDispatcher._instance.AddAgent(this.agentName, this.GetId());
        }

        public void ChangeState(State<Bob> state)
        {
            this.stateMachine.ChangeState(state);
        }

        public override void RevertToPreviousState()
        {
            this.stateMachine.RevertToPreviousState();
        }

        new public void Update()
        {
            this.stateMachine.Update();
        }

        public override bool HandleMessage(Message m)
        {
            return stateMachine.HandleMessage(m);
        }
/*
        public override bool HandleSenseEvent(SenseNotification not)
        {
            return stateMachine.HandleSenseEvent(not);
        }

        public override bool DetectsModality(Modality m)
        {
            return false;
        }
*/
    }
}