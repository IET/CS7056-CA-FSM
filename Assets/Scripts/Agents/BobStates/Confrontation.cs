﻿using UnityEngine;
namespace Assets.Scripts
{
    public sealed class Confrontation : State<Bob>
    {
        const float TIMEOUT = 50;
        float time;

        static readonly Confrontation instance = new Confrontation();

        public static Confrontation Instance
        {
            get
            {
                return instance;
            }
        }

        static Confrontation() { }
        private Confrontation() { }

        public override void Enter(Bob agent)
        {
            agent.Speak("Oh no, the outlaw... What does he want?");
            time = 0;
        }

        public override void Execute(Bob agent)
        {
            time += Time.deltaTime;
            //This really shouldn't be possible
            if (time > TIMEOUT)
            {
                Debug.LogError("Bob timed out during a heated confrontation. Something's amiss in the code.");
                agent.RevertToPreviousState();
            }

        }

        public override void Exit(Bob agent)
        {
            agent.Speak("Well damn, why's that Outlaw always bothering me anyway?");
        }

        public override bool OnMessage(Bob agent, Message m)
        {
            switch (m.msg)
            {
                case (int)message_type.msg_trouble:
                    BoardManager bm = GameObject.Find("GameManager").GetComponent<BoardManager>();

                    if (agent.gridPosition == bm.gridMap["shack"])
                    {
                        MessageDispatcher.instance.DispatchMessage(2, agent.GetId(), m.sender, (int)message_type.msg_my_house);
                        agent.Speak("How dare you trouble me in my own home! You should be ashamed!");
                    }
                    else
                    {
                        MessageDispatcher.instance.DispatchMessage(2, agent.GetId(), m.sender, (int)message_type.msg_no_trouble);
                        agent.Speak("Oh no, no trouble for me! No Siree!");
                    }
                    return true;
                case (int)message_type.msg_carry_on:
                    agent.RevertToPreviousState();
                    return true;
                default:
                    return false;
            }
        }

        public override bool OnSenseEvent(Bob agent, SenseNotification not)
        {
            return false;
        }
    }
}