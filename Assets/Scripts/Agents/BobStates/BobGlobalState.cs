﻿using UnityEngine;
namespace Assets.Scripts
{
    public sealed class BobGlobalState : State<Bob>
    {
        const int TARGET_REST = 100;
        int rest;

        static readonly BobGlobalState instance = new BobGlobalState();

        public static BobGlobalState Instance
        {
            get
            {
                return instance;
            }
        }

        static BobGlobalState() { }
        private BobGlobalState() { }

        public override void Enter(Bob agent)
        {

        }

        public override void Execute(Bob agent)
        {

        }

        public override void Exit(Bob agent)
        {

        }

        public override bool OnMessage(Bob agent, Message m)
        {
            switch (m.msg)
            {
                case ((int)message_type.msg_dinner_ready):
                    agent.ChangeState(GoHomeAndRest.Instance);
                    return true;
                case (int)message_type.msg_stop:
                    agent.currentPath.Delete();
                    agent.ChangeState(Confrontation.Instance);
                    return true;
                default:
                    return false;
            }
        }

        public override bool OnSenseEvent(Bob agent, SenseNotification not)
        {
            return false;
        }
    }
}