﻿using UnityEngine;
namespace Assets.Scripts
{
    public sealed class GoHomeAndRest : State<Bob>
    {
        static readonly GoHomeAndRest instance = new GoHomeAndRest();

        float lastAction;
        float timeout;

        public static GoHomeAndRest Instance
        {
            get
            {
                return instance;
            }
        }

        bool hasEaten;

        static GoHomeAndRest() { }
        private GoHomeAndRest() { }

        public override void Enter(Bob agent)
        {
            agent.Speak("Walkin' Home");
            hasEaten = false;
            //TODO - Implement a way to identify other agents in the system
            //MessageDispatcher.instance.DispatchMessage(0, agent.GetId(), Elsa.GetId(), message_type.Msg_HiHoneyImHome);
            //BoardManager bm = GameObject.Find("GameManager").GetComponent<BoardManager>();
            //agent.gridPosition = GameObject.Find("GameManager").GetComponent<BoardManager>().gridMap["shack"];
            agent.MoveToward(GameObject.Find("GameManager").GetComponent<BoardManager>().gridMap["shack"]);

            lastAction = 0.0f;
            timeout = 10.0f;
        }

        public override void Execute(Bob agent)
        {
            if (Time.time - lastAction > timeout)
            {
                lastAction = Time.time;
            }
            else
            {
                return;
            }

            if (agent.fatigue > 0 && agent.gridPosition == GameObject.Find("GameManager").GetComponent<BoardManager>().gridMap["shack"])
            {
                if (!hasEaten)
                {
                    agent.Speak("My oh my, that was a fine feed. Now I best be off to bed.");
                    MessageDispatcher.instance.DispatchMessageByName(0, agent.GetId(), "Elsa", (int)message_type.msg_bedtime);
                    hasEaten = true;
                }
                else
                {
                    agent.Speak("ZZZZzzzz....");
                    agent.fatigue -= Time.deltaTime;
                }
            }
            else if (agent.fatigue <= 0 && agent.gridPosition == GameObject.Find("GameManager").GetComponent<BoardManager>().gridMap["shack"] && agent.currentPath.Count == 0)
            {
                agent.Speak("Feeling rested, time to get back to work!");
                MessageDispatcher.instance.DispatchMessageByName(0, agent.GetId(), "Elsa", (int)message_type.msg_wakeup);
                agent.fatigue = 0.0f;
                agent.ChangeState(WorkState.Instance);
            }
        }

        public override void Exit(Bob agent)
        {
        }

        public override bool OnMessage(Bob agent, Message m)
        {
            return false;
        }

        public override bool OnSenseEvent(Bob agent, SenseNotification not)
        {
            return false;
        }
    }
}