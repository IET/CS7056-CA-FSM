﻿using UnityEngine;
namespace Assets.Scripts
{
    public sealed class WorkState : State<Bob>
    {

        static readonly WorkState instance = new WorkState();


        const float WORK_DIFFICULTY = 2.0f;
        float lastAction;
        float timeout;

        public static WorkState Instance
        {
            get
            {
                return instance;
            }
        }

        static WorkState() { }
        private WorkState() { }

        public override void Enter(Bob agent)
        {
            agent.Speak("Well, back to work with me.");
            //Board may not be initialised yet.
            try {
                agent.MoveToward(GameObject.Find("GameManager").GetComponent<BoardManager>().gridMap["mine"]);
            }
            catch { }
            lastAction = timeout = 0.0f;
        }

        public override void Execute(Bob agent)
        {
            if (Time.time - lastAction > timeout)
            {
                agent.fatigue += Time.deltaTime * WORK_DIFFICULTY;
                lastAction = Time.time;
            }
            else
            {
                return;
            }

            if (agent.gridPosition == GameObject.Find("GameManager").GetComponent<BoardManager>().gridMap["mine"])
            {
                timeout = 5;
                double roll = Random.value * 10;
                
                if (roll > 8.0f)
                {
                    agent.goldNuggets++;
                    agent.Speak("Yipee! I found a shiny gold nugget!");
                }
            }
            else if (agent.currentPath == null || agent.currentPath.Count == 0)
            {
                agent.MoveToward(GameObject.Find("GameManager").GetComponent<BoardManager>().gridMap["mine"]);
            }
        }

        public override void Exit(Bob agent)
        {
            agent.Speak("That's enough time in the mine for one day!");
        }

        public override bool OnMessage(Bob agent, Message m)
        {
            return false;
        }

        public override bool OnSenseEvent(Bob agent, SenseNotification not)
        {
            return false;
        }
    }
}