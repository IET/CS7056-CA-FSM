﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public abstract class Agent : MonoBehaviour
    {
        //Unique agent identifier
        protected int id;

        public GridPosition gridPosition = new GridPosition(-1, -1);

        //Path to current target
        public Path currentPath;

        //TODO - Maybe go back to having this as a singleton. Turns out that wasn't the source of your issues.
        public AStarPathfinder aStarPathfinder = new AStarPathfinder();

        public float movementSpeed;
        [HideInInspector]
        public float currMovementCost;
        //this is the next valid ID. Each time a BaseGameEntity is instantiated
        //this value should be updated
        static int nextValidId = 0;

        public StateMachine<Agent> stateMachine;

        public string agentName;

        public Agent()
        {
            id = nextValidId;
            AgentManager.instance.RegisterAgent(this);
            ++nextValidId;
        }

        public virtual void Execute() { }

        public virtual void Update()
        {
            this.stateMachine.Update();
        }
        
        public virtual void ChangeState(State<Agent> state)
        {
            this.stateMachine.ChangeState(state);
        }

        //Having this virtual with sample implementation was causing issues
        abstract public void RevertToPreviousState();
/*        {
            this.stateMachine.RevertToPreviousState();
        }
*/

        public void Speak(String sentence)
        {
            //DebugConsole.Log(this.name + ": " + sentence);
            Debug.Log(this.name + ": " + sentence);
        }
        public int GetId()
        {
            return id;
        }

        public void MoveToward(GridPosition target)
        {
            if (currentPath != null && currentPath.Count > 0)
                currentPath.Delete();

            currentPath = aStarPathfinder.FindPath(this.gridPosition, target);

            BoardManager bm = GameObject.Find("GameManager").GetComponent<BoardManager>();

            Tile currentTile = bm.worldGrid[gridPosition.x, gridPosition.y].GetComponent<Tile>();
            this.currMovementCost = currentTile.movementCost;
        }

        public void Move(GridPosition target)
        {
            //TODO - Maybe check that target is valid?
            BoardManager bm = GameObject.Find("GameManager").GetComponent<BoardManager>();
            //GameObject targetGO = bm.worldGrid[target.x, target.y];
            //this.gameObject.transform.position = targetGO.transform.position;
            this.gridPosition = target;
            SenseManager.instance.AddSignal(new Signal(new SightModality(), 1000.0f, target, this.id));
        }

 /*       public void Move(Vector3 target)
        {
            this.gameObject.transform.position = new Vector3(target.x, target.y, this.gameObject.transform.z);
        }
*/
        //Presumably this will err if not hidden (see RevertToPrviousState above)
        abstract public bool HandleMessage(Message m);

    }
}
