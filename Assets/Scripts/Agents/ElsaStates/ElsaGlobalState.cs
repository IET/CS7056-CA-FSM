﻿using UnityEngine;

namespace Assets.Scripts
{
    public sealed class ElsaGlobalState : State<Elsa>
    {
        static readonly ElsaGlobalState instance = new ElsaGlobalState();

        public static ElsaGlobalState Instance
        {
            get
            {
                return instance;
            }
        }

        static ElsaGlobalState() { }
        private ElsaGlobalState() { }

        public override void Enter(Elsa agent)
        {

        }

        public override void Execute(Elsa agent)
        {

        }

        public override void Exit(Elsa agent)
        {
            //agent.Speak("I'll check on that in a bit.");
        }

        public override bool OnMessage(Elsa agent, Message m)
        {
            switch(m.msg)
            {
                case ((int)message_type.msg_dinner_ready):
                    agent.Speak("BOB!! Dinner's ready!");
                    MessageDispatcher._instance.DispatchMessageByName(0.0f, agent.GetId(), "Bob", (int)message_type.msg_dinner_ready);
                    agent.isCooking = false;
                    return true;
                case (int)message_type.msg_bedtime:
                    agent.ChangeState(SleepState.Instance);
                    return true;
                default:
                    return false;
            }
        }

        public override bool OnSenseEvent(Elsa agent, SenseNotification not)
        {
            return false;
        }
    }
}