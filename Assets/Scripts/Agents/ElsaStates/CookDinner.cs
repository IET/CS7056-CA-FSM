﻿using UnityEngine;

namespace Assets.Scripts
{
    public sealed class CookDinner : State<Elsa>
    {
        static readonly CookDinner instance = new CookDinner();

        public static CookDinner Instance
        {
            get
            {
                return instance;
            }
        }

        static CookDinner() { }
        private CookDinner() { }

        float time;

        public override void Enter(Elsa agent)
        {
            if (! agent.isCooking)
            {
                MessageDispatcher._instance.DispatchMessage(15.0f, agent.GetId(), agent.GetId(), (int)message_type.msg_dinner_ready);
                agent.Speak("Better Shtick the Oven On.");
                agent.isCooking = true;
                time = Time.time;
            }
            else
            {
                time = 0.0f;
            }
        }

        public override void Execute(Elsa agent)
        {
            if (Time.time - time > 5)
                agent.RevertToPreviousState();
        }

        public override void Exit(Elsa agent)
        {
            //agent.Speak("I'll check on that in a bit.");
        }

        public override bool OnMessage(Elsa agent, Message m)
        {
            return false;
        }

        public override bool OnSenseEvent(Elsa agent, SenseNotification not)
        {
            return false;
        }
    }
}