﻿using UnityEngine;

namespace Assets.Scripts
{
    public sealed class SleepState : State<Elsa>
    {
        static readonly SleepState instance = new SleepState();

        float lastAction;
        float timeout;

        public static SleepState Instance
        {
            get
            {
                return instance;
            }
        }

        static SleepState() { }
        private SleepState() { }

        public override void Enter(Elsa agent)
        {
            timeout = 10.0f;
        }

        public override void Execute(Elsa agent)
        {
            if (Time.time - lastAction > timeout)
            {
                lastAction = Time.time;
            }
            else
            {
                return;
            }

            agent.Speak("ZZZZzzzz....");
            
        }

        public override void Exit(Elsa agent)
        {
            agent.Speak("Ahh, another beautiful day in this barren desert!");
        }

        public override bool OnMessage(Elsa agent, Message m)
        {
            switch ((int)m.msg)
            {
                case (int)message_type.msg_wakeup:
                    agent.RevertToPreviousState();
                    return true;
                default:
                    return false;
            }
        }

        public override bool OnSenseEvent(Elsa agent, SenseNotification not)
        {
            return false;
        }
    }
}