﻿using UnityEngine;

namespace Assets.Scripts
{
    public sealed class DoHouseWork : State<Elsa>
    {
        const float DINNER_TIMEOUT = 30;

        static readonly DoHouseWork instance = new DoHouseWork();
        float timeout;
        float lastAction;

        float lastDinner;
        public static DoHouseWork Instance
        {
            get
            {
                return instance;
            }
        }

        static DoHouseWork() { }
        private DoHouseWork() { }

        public override void Enter(Elsa agent)
        {
            agent.Speak("Time to do some housework.");
            timeout = 0;
            lastAction = 0;
        }

        public override void Execute(Elsa agent)
        {
            if (Time.time - lastAction > timeout)
            {
                lastAction = Time.time;
            }
            else
            {
                return;
            }

            double chore = Random.value * 30;

            if (chore > 20.0)
            {
                agent.Speak("Moppin' the floor.");
                timeout = 30.0f;
            }
            else if (chore > 10.0)
            {
                agent.Speak("Washin' the dishes.");
                timeout = 40.0f;
            }
            else
            {
                agent.Speak("Makin' the bed.");
                timeout = 20.0f;
            }

            double check = Random.value;

            if (Time.time - lastDinner > DINNER_TIMEOUT)
            {
                agent.ChangeState(CookDinner.Instance);
                lastDinner = Time.time;
            }
            else if (check > 0.9)
            {
                agent.ChangeState(VisitBathroom.Instance);
                timeout = 45.0f;
            }
        }

        public override void Exit(Elsa agent)
        {
        }

        public override bool OnMessage(Elsa agent, Message m)
        {
            return false;
        }

        public override bool OnSenseEvent(Elsa agent, SenseNotification not)
        {
            return false;
        }
    }
}