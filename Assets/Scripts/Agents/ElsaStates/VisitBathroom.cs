﻿using UnityEngine;

namespace Assets.Scripts
{
    public sealed class VisitBathroom : State<Elsa>
    {
        static readonly VisitBathroom instance = new VisitBathroom();

        public static VisitBathroom Instance
        {
            get
            {
                return instance;
            }
        }

        static VisitBathroom() { }
        private VisitBathroom() { }

        public override void Enter(Elsa agent)
        {
            agent.Speak("Going to the bathroom");
        }

        public override void Execute(Elsa agent)
        {
            agent.Speak("Dropping deuce.");

            agent.RevertToPreviousState();
        }

        public override void Exit(Elsa agent)
        {
            agent.Speak("Whew... I'd give that a minute....");
        }

        public override bool OnMessage(Elsa agent, Message m)
        {
            return false;
        }

        public override bool OnSenseEvent(Elsa agent, SenseNotification not)
        {
            return false;
        }
    }
}