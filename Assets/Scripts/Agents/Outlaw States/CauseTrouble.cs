using UnityEngine;

namespace Assets.Scripts
{
    public sealed class CauseTrouble : State<Outlaw>
    {
        static readonly CauseTrouble instance = new CauseTrouble();

        const float TIMEOUT = 120;

        public static CauseTrouble Instance
        {
            get
            {
                return instance;
            }
        }

        static CauseTrouble() { }
        private CauseTrouble() { }

        float time;

        public override void Enter(Outlaw agent)
        {
            agent.Speak("You lookin' for trouble, Rube?");
            MessageDispatcher.instance.DispatchMessage(2, agent.GetId(), agent.target, (int)message_type.msg_trouble);
            time = 0;
        }

        public override void Execute(Outlaw agent)
        {
            time += Time.deltaTime;
            //This really shouldn't be possible
            if (time > TIMEOUT)
            {
                agent.RevertToPreviousState();
                Debug.LogError("Outlaw timed out causing trouble. Something's amiss in the code.");
            }

            if (GridPosition.ManhattanDistance( agent.gridPosition,  AgentManager.instance.GetAgentWithId(agent.target).gridPosition) > 1)
                agent.ChangeState(GoDrinking.Instance);

        }

        public override void Exit(Outlaw agent)
        {
            agent.Speak("Aw shucks, nobody's ever looking for trouble...");
        }

        public override bool OnMessage(Outlaw agent, Message m)
        {
            switch(m.msg)
            {
                case (int)message_type.msg_no_trouble:
                case (int)message_type.msg_my_house:
                    MessageDispatcher.instance.DispatchMessage(2, agent.GetId(), m.sender, (int)message_type.msg_carry_on);
                    agent.Speak("Oh, I'm sorry. Carry on then!");
                    agent.ChangeState(GoDrinking.Instance);
                    return true;
                default:
                    return false;
            }
        }
        public override bool OnSenseEvent(Outlaw agent, SenseNotification not)
        {
            return false;
        }
    }
}