using UnityEngine;

namespace Assets.Scripts
{
    public sealed class GoDrinking : State<Outlaw>
    {
        static readonly GoDrinking instance = new GoDrinking();

        const float BASE_THIRST = 50;

        public static GoDrinking Instance
        {
            get
            {
                return instance;
            }
        }

        static GoDrinking() { }
        private GoDrinking() { }

        float thirst;
        GridPosition barLocation;

        float lastDrinkTime = 0.0f;

        public override void Enter(Outlaw agent)
        {
            thirst = BASE_THIRST;
            agent.Speak("That bar ain't gonna know what hit it.");
            barLocation = GameObject.Find("GameManager").GetComponent<BoardManager>().gridMap["bar"];
            agent.MoveToward(barLocation);
        }

        public override void Execute(Outlaw agent)
        {
            //If we reached the bar
            if (agent.gridPosition == barLocation)
            {
                thirst -= (float)Random.Range(1, 10) * Time.deltaTime;

                if (Time.time - lastDrinkTime > agent.invDrinkingSpeed)
                {
                    lastDrinkTime = Time.time;
                    agent.Speak("Another whiskey, barkeep.");
                }
            }
            if (thirst <= 0)
                agent.ChangeState(WanderState.Instance);
        }

        public override void Exit(Outlaw agent)
        {
            agent.Speak("Outlaw: Better go cause some trouble");
        }

        public override bool OnMessage(Outlaw agent, Message m)
        {
            return false;
        }
        public override bool OnSenseEvent(Outlaw agent, SenseNotification not)
        {
            return false;
        }
    }
}