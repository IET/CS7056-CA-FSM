﻿using UnityEngine;

namespace Assets.Scripts
{
    public sealed class OutlawGlobalState : State<Outlaw>
    {
        static readonly OutlawGlobalState instance = new OutlawGlobalState();

        public static OutlawGlobalState Instance
        {
            get
            {
                return instance;
            }
        }

        static OutlawGlobalState() { }
        private OutlawGlobalState() { }

        public override void Enter(Outlaw agent)
        {

        }

        public override void Execute(Outlaw agent)
        {

        }

        public override void Exit(Outlaw agent)
        {
        }

        public override bool OnMessage(Outlaw agent, Message m)
        {
            switch(m.msg)
            {
                case ((int)message_type.msg_dinner_ready):
                    //MessageDispatcher._instance.DispatchMessageByName(0.0f, agent.GetId(), "Bob", (int)message_type.msg_dinner_ready);
                    //agent.isCooking = false;
                    return true;
                default:
                    return false;
            }
        }
        public override bool OnSenseEvent(Outlaw agent, SenseNotification not)
        {
            //Debug.Log("THE OUTLAW SAW YOU!!");
            return true;
        }
    }
}