using UnityEngine;

namespace Assets.Scripts
{
    public sealed class SeekState : State<Outlaw>
    {
        static readonly SeekState instance = new SeekState();

        const int THIRST_THRESHOLD = 100;

        public static SeekState Instance
        {
            get
            {
                return instance;
            }
        }

        static SeekState() { }
        private SeekState() { }

        public override void Enter(Outlaw agent)
        {
            agent.Speak("Ahah! I saw a rube, I'll go check that out.");
        }

        public override void Execute(Outlaw agent)
        {
    
            if (agent.gridPosition == AgentManager.instance.GetAgentWithId(agent.target).gridPosition)
            {
                MessageDispatcher.instance.DispatchMessage(0, agent.GetId(), agent.target, (int)message_type.msg_stop);
                agent.ChangeState(CauseTrouble.Instance);
            }
            else if (agent.currentPath.Count == 0)
            {
                agent.Speak("Shucks, looks like the rube made a break for it. Well, back to the bar with me.");
                agent.ChangeState(GoDrinking.Instance);
            }

        }

        public override void Exit(Outlaw agent)
        {
        }

        public override bool OnMessage(Outlaw agent, Message m)
        {
            return false;
        }
        public override bool OnSenseEvent(Outlaw agent, SenseNotification not)
        {
            if (AgentManager.instance.GetAgentWithId(not.signal.sourceId).name != "Elsa")
            {
                agent.target = not.signal.sourceId;
                agent.currentPath.Delete();
                agent.currentPath = agent.aStarPathfinder.FindPath(agent.gridPosition, AgentManager.instance.GetAgentWithId(not.signal.sourceId).gridPosition);
            }
            else
            {
                agent.Speak("Outlaw: I doubt the broad has anything on her. Not worth my time.");
            }
            return true;
        }
    }
}