using UnityEngine;

namespace Assets.Scripts
{
    public sealed class WanderState : State<Outlaw>
    {
        static readonly WanderState instance = new WanderState();

        const int THIRST_THRESHOLD = 250;
        int thirst;
        int lastDirection;

        public static WanderState Instance
        {
            get
            {
                return instance;
            }
        }

        static WanderState() { }
        private WanderState() { }

        GridPosition targetNode;
        
        public override void Enter(Outlaw agent)
        {
            if (agent.gridPosition.x != -1 && agent.gridPosition.y != -1)
                targetNode = FindNewTarget(agent);
            thirst = 0;
        }

        public override void Execute(Outlaw agent)
        {
            if (agent.gridPosition.x == -1 && agent.gridPosition.y == -1)
                return;

            if (agent.currentPath.Count == 0)
            {
                if (agent.currMovementCost <= 0)
                {
                    thirst += (int)Random.Range(1, 10);

                    //Do wandering
                    targetNode = FindNewTarget(agent);

                    BoardManager bm = GameObject.Find("GameManager").GetComponent<BoardManager>();
                    //Probably shouldn't require pathfinding for this but the movement does currently require a path...
                    //agent.MoveToward(target);
                    Tile targetTile = bm.worldGrid[targetNode.x, targetNode.y].GetComponent<Tile>();
                    agent.Move(targetNode);
                    agent.currMovementCost = targetTile.movementCost;
                }
                else
                {
                    agent.currMovementCost -= agent.movementSpeed * Time.deltaTime;
                }
            }
            else
            {
                agent.currentPath.Delete();
            }
            if (thirst > THIRST_THRESHOLD)
            {
                agent.Speak("Whew! Really working up a thirst in this desert heat!");
                agent.ChangeState(GoDrinking.Instance);
            }
        }

        public override void Exit(Outlaw agent)
        {
        }

        public override bool OnMessage(Outlaw agent, Message m)
        {
            return false;
        }

        public override bool OnSenseEvent(Outlaw agent, SenseNotification not)
        {
            if (AgentManager.instance.GetAgentWithId(not.signal.sourceId).name != "Elsa")
            {
                agent.target = not.signal.sourceId;
                agent.currentPath.Delete();
                agent.currentPath = agent.aStarPathfinder.FindPath(agent.gridPosition, AgentManager.instance.GetAgentWithId(not.signal.sourceId).gridPosition);
                agent.ChangeState(SeekState.Instance);
            }
            else
            {
                agent.Speak("I doubt the broad has anything on her. Not worth my time.");
            }
            return true;
        }

        GridPosition FindNewTarget(Agent agent)
        {
            GridPosition targetPos = new GridPosition(agent.gridPosition);

            int direction = (int)Random.Range(0, 3);

            if (direction == lastDirection)
                direction = (direction + 1) % 4;
            lastDirection = direction;
            switch (direction)
            {
                case 0:
                    ++targetPos.x;
                    break;
                case 1:
                    ++targetPos.y;
                    break;
                case 2:
                    --targetPos.x;
                    break;
                case 3:
                    --targetPos.y;
                    break;
            }

            //If it's not valid, push away from edges
            if (!targetPos.isValid())
            {
                BoardManager bm = GameObject.Find("GameManager").GetComponent<BoardManager>();

                if (targetPos.y > bm.rows -3)
                {
                    --targetPos.y;
                }
                else if (targetPos.y < 3)
                {
                    ++targetPos.y;
                }

                if (targetPos.x > bm.columns -3)
                {
                    --targetPos.x;
                }
                else if (targetPos.x < 3)
                {
                    ++targetPos.x;
                }

            }
            return targetPos;
        }
    }
}